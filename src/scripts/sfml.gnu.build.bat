mkdir gnu.%1.artefacts
pushd gnu.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

mkdir build
pushd build
	cmake ../source -G"MinGW Makefiles" -DCMAKE_BUILD_TYPE=%1
	cmake --build .
popd build

xcopy source\include\* gnu.%1.artefacts\include\ /y /s
xcopy build\lib\*.a gnu.%1.artefacts\lib\ /y /s
xcopy build\lib\*.dll gnu.%1.artefacts\bin\ /y /s
xcopy source\extlibs\bin\x64\*.dll gnu.%1.artefacts\bin\ /y /s
copy source\version.txt gnu.%1.artefacts\sfml.gnu.%1.version.txt

rd build /s /q
