mkdir gnu.%1.artefacts
pushd gnu.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

xcopy source\bandit\* gnu.%1.artefacts\include\bandit\ /y /s
copy source\version.txt gnu.%1.artefacts\bandit.gnu.%1.version.txt
