mkdir gnu.%1.artefacts
pushd gnu.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

xcopy source\asio\include\* gnu.%1.artefacts\include\ /y /s
copy source\version.txt gnu.%1.artefacts\asio.gnu.%1.version.txt

rd build /s /q
