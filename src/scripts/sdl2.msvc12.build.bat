mkdir msvc12.%1.artefacts
pushd msvc12.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

mkdir build
pushd build
	cmake ../source -G"Visual Studio 12" -DSDL_SHARED=ON -DSDL_STATIC=OFF
	msbuild SDL2.sln /p:Configuration=%1 /m:10
popd build

xcopy source\include\* msvc12.%1.artefacts\include\ /y /s
xcopy build\%1\*.lib msvc12.%1.artefacts\lib\ /y /s
xcopy build\%1\*.pdb msvc12.%1.artefacts\lib\ /y /s
xcopy build\%1\*.dll msvc12.%1.artefacts\bin\ /y /s
copy build\include\SDL_config.h msvc12.%1.artefacts\include\SDL_config.h /y

copy source\version.txt msvc12.%1.artefacts\sdl2.msvc12.%1.version.txt /y

rd build /s /q
