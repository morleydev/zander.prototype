mkdir msvc12.%1.artefacts
pushd msvc12.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

mkdir build
pushd build
	cmake ../source -G"Visual Studio 12"
	"C:\Program Files (x86)\MSBuild\12.0\Bin\amd64\MSBuild.exe" SFML.sln /p:Configuration=%1 /m:10
popd build

xcopy source\include\* msvc12.%1.artefacts\include\ /y /s
xcopy build\lib\%1\*.lib msvc12.%1.artefacts\lib\ /y /s
xcopy build\lib\%1\*.pdb msvc12.%1.artefacts\lib\ /y /s
xcopy build\lib\%1\*.dll msvc12.%1.artefacts\bin\ /y /s
xcopy source\extlibs\bin\x86\*.dll msvc12.%1.artefacts\bin\ /y /s
copy source\version.txt msvc12.%1.artefacts\sfml.msvc12.%1.version.txt

rd build /s /q
