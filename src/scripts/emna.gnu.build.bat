mkdir gnu.%1.artefacts
pushd gnu.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

mkdir build
pushd build
	cmake ../source -G"MinGW Makefiles" -DCMAKE_BUILD_TYPE=%1 -DCMAKE_INSTALL_PREFIX=..\gnu.%1.artefacts -DDISABLE_SDL=ON
	cmake --build . && cmake --build . -- install
popd build

copy source\version.txt gnu.%1.artefacts\emna.gnu.%1.version.txt

rd build /s /q
