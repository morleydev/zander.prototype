mkdir msvc12.%1.artefacts
pushd msvc12.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

xcopy source\bandit\* msvc12.%1.artefacts\include\bandit\ /y /s
copy source\version.txt msvc12.%1.artefacts\bandit.msvc12.%1.version.txt
