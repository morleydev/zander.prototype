mkdir gnu.%1.artefacts
pushd gnu.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

mkdir build
pushd build
	cmake ../source -G"MinGW Makefiles" -DCMAKE_BUILD_TYPE=%1 -DSDL_SHARED=ON -DSDL_STATIC=OFF
	cmake --build .
popd build

xcopy source\include\* gnu.%1.artefacts\include\ /y /s
copy build\include\SDL_config.h gnu.%1.artefacts\include\SDL_config.h /y
copy build\libSDL2.a gnu.%1.artefacts\lib\libSDL2.a /y
copy build\libSDL2.dll.a gnu.%1.artefacts\lib\libSDL2.dll.a /y
copy build\libSDL2main.a gnu.%1.artefacts\lib\libSDL2main.a /y
copy build\libSDL2.dll gnu.%1.artefacts\bin\libSDL2.dll /y
copy source\version.txt gnu.%1.artefacts\sdl2.gnu.%1.version.txt /y

rd build /s /q
