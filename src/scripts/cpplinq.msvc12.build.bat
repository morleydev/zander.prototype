mkdir msvc12.%1.artefacts
pushd msvc12.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

xcopy source\CppLinq\* msvc12.%1.artefacts\include\ /y /s

copy source\version.txt msvc12.%1.artefacts\cpplinq.msvc12.%1.version.txt
