mkdir gnu.%1.artefacts
pushd gnu.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

xcopy source\CppLinq\* gnu.%1.artefacts\include\ /y /s
copy source\version.txt gnu.%1.artefacts\cpplinq.gnu.%1.version.txt
