mkdir msvc11.%1.artefacts
pushd msvc11.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

mkdir build
pushd build
	cmake ../source -G"Visual Studio 11"
	"C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe" SFML.sln /p:Configuration=%1 /m:10
popd build

xcopy source\include\* msvc11.%1.artefacts\include\ /y /s
xcopy build\lib\%1\*.lib msvc11.%1.artefacts\lib\ /y /s
xcopy build\lib\%1\*.pdb msvc11.%1.artefacts\lib\ /y /s
xcopy build\lib\%1\*.dll msvc11.%1.artefacts\bin\ /y /s
xcopy source\extlibs\bin\x86\*.dll msvc11.%1.artefacts\bin\ /y /s
copy source\version.txt msvc11.%1.artefacts\sfml.msvc11.%1.version.txt

rd build /s /q
