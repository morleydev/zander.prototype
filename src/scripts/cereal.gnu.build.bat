mkdir gnu.%1.artefacts
pushd gnu.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

xcopy source\include\* gnu.%1.artefacts\include\ /y /s
copy source\version.txt gnu.%1.artefacts\cereal.gnu.%1.version.txt
