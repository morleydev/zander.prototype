mkdir msvc12.%1.artefacts
pushd msvc12.%1.artefacts
	mkdir include
	mkdir lib
	mkdir bin
popd

mkdir build
pushd build
	cmake ../source -G"Visual Studio 12" -DCMAKE_INSTALL_PREFIX=..\msvc12.%1.artefacts
	"C:\Program Files (x86)\MSBuild\12.0\Bin\amd64\MSBuild.exe" UnitTest11.sln /p:Configuration=%1 /m:10
	"C:\Program Files (x86)\MSBuild\12.0\Bin\amd64\MSBuild.exe" INSTALL.vcxproj /p:Configuration=%1
popd build

copy source\version.txt msvc12.%1.artefacts\unittest11.msvc12.%1.version.txt

rd build /s /q
