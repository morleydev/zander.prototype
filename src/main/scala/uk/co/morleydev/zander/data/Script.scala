package zander.client.data

import java.io.{FileNotFoundException, InputStreamReader, BufferedReader, File}
import zander.client.Directories

class Script(scriptName : String) {

    val fullScriptPath = Directories.scriptPath + File.separator + scriptName + ".bat"

    def run(runningDirectory : File, arguments : String*) {

        println("Running script " + fullScriptPath)

        if ( !new File(fullScriptPath).exists )
            throw new FileNotFoundException(fullScriptPath)

        val processBuilder = new ProcessBuilder()

        processBuilder.command("C:\\Windows\\System32\\cmd.exe", "/c", fullScriptPath, arguments.toList.mkString(" "))
        processBuilder.directory(runningDirectory)

        val process = processBuilder.start()
        val bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream))
        Iterator.continually(bufferedReader.readLine())
                .takeWhile(_ != null)
                .foreach(println(_))
        process waitFor()
    }
}
