package zander.client.data

import zander.client.models.BuildMode.BuildMode
import zander.client.models.Compiler.Compiler
import java.io.{PrintWriter, File}
import zander.client.Directories
import scala.io.{BufferedSource, Source}

class ArtefactList(projectName : String, compiler : Compiler, mode : BuildMode) {

    private val artefactListFilename = "%s.%s.%s.artefacts.txt".format(projectName, compiler, mode)

    def saveToCache(artefacts : Seq[String]) {

        val cacheDirectory = new File((Directories.cachePath + File.separator + "%s").format(projectName))
        val artefactDirectory = new File("%s%s%s.%s.artefacts".format(cacheDirectory, File.separator, compiler, mode))

        val fileStream = new PrintWriter("%s%s%s".format(artefactDirectory, File.separator, artefactListFilename), "UTF-8")
        artefacts.foreach(x => fileStream.println(x))
        fileStream.close()
    }

    def readFromLocal : Array[String] =
        {
            val file = Source.fromFile(artefactListFilename)
            val result = file.getLines().toArray
            file.close()
            result
        }

    def localExists : Boolean =
        new File(artefactListFilename).exists

    def deleteLocal() = {

        readFromLocal.foreach(new File(_).delete())
        
        val artefactListFile = new File(artefactListFilename)
        if ( !artefactListFile.delete() )
            artefactListFile.deleteOnExit()
    }
}
