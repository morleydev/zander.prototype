package zander.client.services

import java.io.{FileOutputStream, FileInputStream, FileNotFoundException, File}

class FileSystemService {

    def listFiles(directoryName : String) : List[String] = {

        val directory = new File(directoryName)
        val files = directory.listFiles()
        var result = List[String]()

        files.foreach((file: File) => {
            if (file.isFile)
                result = result ::: List(file.getAbsolutePath)
            else
                result = result ::: listFiles(file.getAbsolutePath)
        })
        result
    }

    def deleteEmptyDirectories() {

        val directory = new File(".")
        val files = directory.listFiles()

        files.foreach((file: File) => {
            if (!file.isFile)
                recursiveDeleteEmptyDirectories(file.getAbsolutePath)
        })
    }

    private def recursiveDeleteEmptyDirectories(directoryName : String) {

        val directory = new File(directoryName)
        val files = directory.listFiles()

        var deleteMe = true
        files.foreach((file: File) => {
            if (file.isFile)
                deleteMe = false
            else
                recursiveDeleteEmptyDirectories(file.getAbsolutePath)
        })
        if ( deleteMe )
            directory.delete()
    }

    def deleteFile(file : File) {

        if ( !file.exists() )
            throw new FileNotFoundException()
        file.delete()
    }

    def copyFiles(sourceLocation: File, targetLocation: File) {

        if ( !sourceLocation.exists() )
            throw new FileNotFoundException(sourceLocation.getAbsolutePath)

        if (sourceLocation.isDirectory) {

            if (!targetLocation.exists())
                targetLocation.mkdir()

            sourceLocation.list.foreach(child => copyFiles(new File(sourceLocation, child), new File(targetLocation, child)))
        } else {

            val in = new FileInputStream(sourceLocation)
            val out = new FileOutputStream(targetLocation)
            val buf = new Array[Byte](1024)

            Iterator.continually({ in.read(buf) })
                .takeWhile(_ > 0)
                .foreach(out.write(buf, 0, _))

            in.close()
            out.close()
        }
    }

}
