package zander.client.services

import zander.client.data.Script
import java.io.File
import zander.client.Directories
import zander.client.models.Compiler.Compiler
import zander.client.models.BuildMode.BuildMode
import scala.io.Source

class CacheService(fileSystemService : FileSystemService) {

    def cacheExists(projectName : String) : Boolean =
        new File(cachePath(projectName)).exists

    def downloadSource(projectName : String) = {

        val cacheDirectory = new File(cachePath(projectName))
        cacheDirectory.mkdirs()

        new Script("%s.download".format(projectName)).run(cacheDirectory)
    }

    def updateSource(projectName : String) = {

        val cacheDirectory = new File(cachePath(projectName))
        new Script("%s.update".format(projectName)).run(cacheDirectory)
    }

    def readSourceVersion(projectName : String) : String = {

        val sourceVersionFilePath = "%s%ssource%sversion.txt".format(cachePath(projectName), File.separator, File.separator)
        val file = Source.fromFile(new File(sourceVersionFilePath))
        val result = file.mkString
        file.close()
        result
    }

    def artefactsExist(projectName : String, compiler : Compiler, mode : BuildMode) : Boolean =
        new File(artefactsPath(projectName, compiler, mode)).exists

    def buildArtefacts(projectName : String, compiler : Compiler, mode : BuildMode) : Seq[String] = {

        val artefactDirectory = new File(artefactsPath(projectName, compiler, mode))
        if ( artefactDirectory.exists )
            artefactDirectory.delete()

        val buildScript = new Script("%s.%s.build".format(projectName, compiler))
        buildScript.run(new File(cachePath(projectName)), mode.toString)

        fileSystemService.listFiles(artefactDirectory.getAbsolutePath).map(x => x.substring(artefactDirectory.getAbsolutePath.length() + 1))
    }

    def readArtefactsVersion(projectName : String, compiler : Compiler, mode : BuildMode) : String = {

        val artefactsVersionFilePath = "%s%s%s.%s.%s.version.txt".format(artefactsPath(projectName, compiler, mode),
                                                                         File.separator,
                                                                         projectName,
                                                                         compiler,
                                                                         mode)
        val file = Source.fromFile(new File(artefactsVersionFilePath))
        val result = file.mkString
        file.close()
        result
    }

    def copyArtefacts(projectName : String, compiler : Compiler, mode : BuildMode, destination : File) =
        fileSystemService.copyFiles(new File(artefactsPath(projectName, compiler, mode)), destination)

    def cachePath(projectName : String) : String =
        (Directories.cachePath + File.separator + "%s").format(projectName)

    def artefactsPath(projectName : String, compiler : Compiler, mode : BuildMode) : String =
        "%s%s%s.%s.artefacts".format(cachePath(projectName), File.separator, compiler, mode)
}
