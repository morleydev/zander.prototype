package zander.client

import java.io.File

object Directories {

    private val rootPath = {

        new File(getClass
            .getProtectionDomain
            .getCodeSource
            .getLocation
            .getPath)
            .getParentFile
            .getPath
            .replace('/', '\\')
    }

    val scriptPath = ("%s" + File.separator + "scripts").format(rootPath)
    val cachePath = ("%s" + File.separator + "cache").format(rootPath)
}
