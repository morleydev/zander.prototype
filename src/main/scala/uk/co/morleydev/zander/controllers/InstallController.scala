package zander.client.controllers

import java.io._
import zander.client.data.{ArtefactList, Script}
import zander.client.Directories
import zander.client.models.BuildMode
import zander.client.models.BuildMode.BuildMode
import zander.client.models.Compiler.Compiler
import zander.client.services.{CacheService, FileSystemService}

class InstallController(cacheService : CacheService) {

    def run(projectName : String, compiler : Compiler, mode : BuildMode) {

        val artefactList = new ArtefactList(projectName, compiler, mode)
        if ( artefactList.localExists )
            return

        if (!cacheService.cacheExists(projectName)) {
            println("Library source not presently in cache...")
            cacheService.downloadSource(projectName)
        } else {
            println("Library source presently in cache, attempting update...")
            cacheService.updateSource(projectName)
        }

        val cachedSourceVersion = cacheService.readSourceVersion(projectName)
        val artefactVersion = {
            if (cacheService.artefactsExist(projectName, compiler, mode))
                cacheService.readArtefactsVersion(projectName, compiler, mode)
            else
                ""
        }

        if (cachedSourceVersion != artefactVersion) {

            println("Desired artefacts not presently in cache or not latest version...")

            val artefacts = cacheService.buildArtefacts(projectName, compiler, mode)
            artefactList.saveToCache(artefacts)
        }

        println("Moving artefacts to current directory...")
        cacheService.copyArtefacts(projectName, compiler, mode, new File("."))
        println("...Moved artefacts to current directory")
    }
}
