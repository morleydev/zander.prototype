package zander.client.controllers

import zander.client.models.Compiler._
import zander.client.models.BuildMode._
import zander.client.data.ArtefactList

class GetController(installController : InstallController, updateController : UpdateController) {

    def run(projectName : String, compiler : Compiler, mode : BuildMode) {

        val artefactList = new ArtefactList(projectName, compiler, mode)
        if ( artefactList.localExists )
        {
            println("Local artefacts exist: Performing update")
            updateController.run(projectName, compiler, mode)
        }
        else
        {
            println("Local artefacts do not exist: Performing install")
            installController.run(projectName, compiler, mode)
        }
    }

}
