package zander.client.controllers

import zander.client.services.{FileSystemService, CacheService}
import zander.client.models.Compiler._
import zander.client.models.BuildMode._
import zander.client.data.ArtefactList
import scala.io.Source
import java.io.File

class UpdateController(cacheService : CacheService, fileSystemService : FileSystemService) {

    def run(projectName : String, compiler : Compiler, mode : BuildMode) {

        val artefactList = new ArtefactList(projectName, compiler, mode)
        if ( !artefactList.localExists )
        {
            println("Local Artefacts do not exist")
            return
        }


        if ( !cacheService.artefactsExist(projectName, compiler, mode) )
        {
            println("Artefacts in cache do not exist")
            return
        }

        val file = Source.fromFile("%s.%s.%s.version.txt".format(projectName, compiler, mode))
        val currentVersion = file.mkString
        file.close()

        var cachedArtefactsVersion = cacheService.readArtefactsVersion(projectName, compiler, mode)

        println("Current local artefacts: " + currentVersion)
        println("Current cached artefacts: " + cachedArtefactsVersion)

        println("Attempting to update source")
        cacheService.updateSource(projectName)
        val cachedSourceVersion = cacheService.readSourceVersion(projectName)
        println("Current cached source version: " + cachedSourceVersion)

        if ( cachedArtefactsVersion != cachedSourceVersion )
        {
            println("Updating cached artefacts")

            val artefacts = cacheService.buildArtefacts(projectName, compiler, mode)
            artefactList.saveToCache(artefacts)
            cachedArtefactsVersion = cacheService.readArtefactsVersion(projectName, compiler, mode)
        }

        if ( cachedArtefactsVersion != currentVersion )
        {
            println("Updating local artefacts")

            artefactList.deleteLocal()
            fileSystemService.deleteEmptyDirectories()

            println("Moving artefacts to current directory...")
            cacheService.copyArtefacts(projectName, compiler, mode, new File("."))
            println("...Moved artefacts to current directory")
        }
    }
}
