package zander.client.controllers

import zander.client.models.Compiler._
import zander.client.models.BuildMode._
import zander.client.data.ArtefactList
import zander.client.services.FileSystemService

class PurgeController(fileSystemService : FileSystemService) {

    def run(projectName : String, compiler : Compiler, mode : BuildMode) {

        val artefactList = new ArtefactList(projectName, compiler, mode)
        if ( !artefactList.localExists )
            return

        println("Deleting local artefacts")
        artefactList.deleteLocal()
        fileSystemService.deleteEmptyDirectories()
    }

}
