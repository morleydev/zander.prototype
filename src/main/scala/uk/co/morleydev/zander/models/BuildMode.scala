package zander.client.models

object BuildMode extends Enumeration {

    type BuildMode = Value
    val Debug = Value("debug")
    val Release = Value("release")

}
