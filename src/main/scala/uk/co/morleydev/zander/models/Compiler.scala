package zander.client.models

object Compiler extends Enumeration {

    type Compiler = Value

    val VisualStudio11 = Value("msvc11")
    val VisualStudio12 = Value("msvc12")
    val GnuCxx = Value("gnu")
}
