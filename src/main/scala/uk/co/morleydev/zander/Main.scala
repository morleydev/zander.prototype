package zander.client

import zander.client.controllers.{PurgeController, GetController, UpdateController, InstallController}
import zander.client.models.{Operation, BuildMode, Compiler}
import zander.client.services.{CacheService, FileSystemService}

object Main {

    private def extractArgument(args: Array[String], index: Int): String = {
        if (args.length <= index)
            readLine()
        else {
            println(args(index))
            args(index)
        }
    }

    def main(args : Array[String]) {

        print("Operation: "); val operation = Operation.withName(extractArgument(args, 0))
        print("Project: "); val project = extractArgument(args, 1)
        print("Compiler: "); val compiler = Compiler.withName(extractArgument(args, 2))
        print("Mode: ");  val mode = BuildMode.withName(extractArgument(args, 3))

        val fileSystemService = new FileSystemService()
        val cacheService = new CacheService(fileSystemService)

        val updateController = new UpdateController(cacheService, fileSystemService)
        val installController = new InstallController(cacheService)
        val getController = new GetController(installController, updateController)
        val purgeController = new PurgeController(fileSystemService)

        if ( operation == Operation.Install )
            installController.run(project, compiler, mode)

        else if ( operation == Operation.Update )
            updateController.run(project, compiler, mode)

        else if ( operation == Operation.Get )
            getController.run(project, compiler, mode)

        else if ( operation == Operation.Purge )
            purgeController.run(project, compiler, mode)
    }
}
