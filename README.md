zander.client Prototype
=============

The prototype of the Zander client.

Supported operations
-------------
* get
* install
* update
* purge
	
Known issues
-------------
* If source is not in cache when attempting an update, update will fail
* May leave unused files and folders lying around after updates to artefact cache
